file=${1}
basename=$(basename $file)
outname=$(echo staufen_${basename})
pdfcrop --margins '-900 -2100 -1200 -1000' $file $outname
