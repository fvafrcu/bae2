index.html: index.asciidoc bae.png bae2.png bu_stab.png natwg.png
	asciidoc -a asciimath --backend=slidy index.asciidoc

bae.png: bae.dot
	dot -Tpng -o bae.png bae.dot

bae2.png: bae2.dot
	dot -Tpng -o bae2.png bae2.dot

bu_stab.png: bu_stab.dot
	dot -Tpng -o bu_stab.png bu_stab.dot

